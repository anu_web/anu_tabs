Drupal.acton_tabs = Drupal.tabs || {};

Drupal.behaviors.tabs = function (context) {

  // Process custom tabs.
  var selected = null;
  $('.drupal-tabs', context)
    .tabs({
      // Add the 'active' class when showing tabs and remove it from siblings.
      show: function(event, ui) {
        //$(ui.tab).parent('li').addClass('pagetabs-select').siblings('li').removeClass('pagetabs-select');
        $('.ui-tabs .pagetabs-select').removeClass('pagetabs-select');
        $(ui.tab).addClass('pagetabs-select');
      }

    })
    $('.ui-tabs-nav.primary').removeClass('primary').addClass('top-level');
    $('.ui-tabs.nav.secondary').removeClass('primary').addClass('second-level');
};